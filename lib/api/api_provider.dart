import 'dart:convert';

import 'package:flop_edt_app/models/resources/Incident.dart';
import 'package:flop_edt_app/models/resources/user.dart';
import 'package:flop_edt_app/models/resources/course.dart';
import 'package:flop_edt_app/models/resources/room.dart';
import 'package:flop_edt_app/models/resources/etablissement.dart';
import 'package:flop_edt_app/models/resources/promotion.dart';
import 'package:flop_edt_app/models/resources/tutor.dart';
import 'package:flop_edt_app/models/state/settings.dart';
import 'package:http/http.dart' as http;

import '../exception/AppException.dart';

///Classe permettant d'intéragir avec l'API.
///La clé d'API est chargée depuis le fichier d'environnement .env.
class APIProvider {
  ///Clé de l'API
  String _key;

  ///Base de l'API
  String _apiBase;

  Map<String, String> _headers = Map.fromEntries([
    MapEntry("accept", "application/json"),
    MapEntry("content-type", "application/json")
  ]);

  APIProvider() {
    /*
    this._key = DotEnv().env['API_KEY'];
    */

    //this._apiBase = DotEnv().env['API_BASE'];
    this._apiBase = getapiUrl();
    //_apiBase = "";
  }

  String get _apiUrl => getapiUrl();
  String getapiUrl() {
    String url;
    getAPIBase().then((val) {
      url = val + "fr/api/";
    });
    return url;
  }

  ///Effectue une requête sur l'API afin de récupérer la liste des cours.
  ///[year] correspond à l'année
  ///[week] correspond au numéro de la semaine
  ///[promo] correspond à la promotion choisie (INFO2, INFO1...)
  ///[department] correspond au département (INFO, CS...)
  ///[group] correspond au groupe choisi (3A, 1A...)
  Future<List<Cours>> getCourses(
      {int year, week, String promo, department, group}) async {
    Settings settings = await Settings.getConfiguration();
    final url = settings.etablissement.url +
        "fr/api/" +
        'fetch/scheduledcourses/?week=$week&year=$year&work_copy=0&dept=$department&train_prog=$promo&group=$group&lineage=true';
    final response = await http.get(Uri.parse(url));

    //Récupération des prof du département
    final urlTutor =
        settings.etablissement.url + "fr/api/" + 'user/tutor/?dept=$department';
    final responseTutors =
        await http.get(Uri.parse(urlTutor), headers: _headers);

    //Récupération de la durée des cours
    final urlTypeCours =
        settings.etablissement.url + "fr/api/courses/type/?dept=$department";
    final responseTypeCours =
        await http.get(Uri.parse(urlTypeCours), headers: _headers);

    if (response.statusCode == 200 &&
        responseTutors.statusCode == 200 &&
        responseTypeCours.statusCode == 200)
      return Cours.createListFromResponses(
          response, responseTutors, responseTypeCours, year, week);
    return <Cours>[];
  }

  /// Récupère l'APIBase
  Future<String> getAPIBase() async {
    Settings settings = await Settings.getConfiguration();
    return settings.etablissement.url + "fr/api/";
  }

  /// Récupère l'ensemble des [Etablissement] disponible.
  Future<List<dynamic>> getEtablissements() async {
    final url = 'https://api.flopedt.org/clients/';
    final response = await http.get(Uri.parse(url), headers: _headers);
    if (response.statusCode == 200)
      return Etablissement.createListFromResponse(response);
    return <dynamic>[];
  }

  /// Récupère une [List] de départements d'un etablissement.
  Future<List<dynamic>> getDepartments() async {
    Settings settings = await Settings.getConfiguration();
    final url = settings.etablissement.url + "fr/api/" + 'fetch/alldepts/';
    final response = await http.get(Uri.parse(url), headers: _headers);
    if (response.statusCode == 200)
      return (jsonDecode(response.body))
          .map((dynamic obj) => (obj as Map<String, dynamic>)["abbrev"])
          .toList();
    return <dynamic>[];
  }

  /// Récupère une [List] de [Cours] d'un [prof] selon un [department], une [year] et une [week].
  Future<List<Cours>> getCoursesOfProf(
      {int year, week, String department, prof}) async {
    Settings settings = await Settings.getConfiguration();
    final url = settings.etablissement.url +
        "fr/api/" +
        'fetch/scheduledcourses/?week=$week&year=$year&tutor_name=$prof&dept=$department&work_copy=0';
    final response = await http.get(Uri.parse(url), headers: _headers);
    //Récupération de la durée des cours
    final urlTypeCours =
        settings.etablissement.url + "fr/api/courses/type/?dept=$department";
    final responseTypeCours =
        await http.get(Uri.parse(urlTypeCours), headers: _headers);

    if (response.statusCode == 200 && responseTypeCours.statusCode == 200)
      return Cours.createListFromResponse(
          response, responseTypeCours, year, week);
    return <Cours>[];
  }

  // Supprime un [Cours] d'un [prof] selon un [id] du [Cours].
  Future<bool> deleteCourseOfProf({int id}) async {
    Settings settings = await Settings.getConfiguration();
    final url =
        settings.etablissement.url + "fr/api/" + 'courses/scheduledcourses/$id';
    final response = await http.delete(Uri.parse(url), headers: _headers);

    if (response.statusCode == 200) return true;
    return false;
  }

  // Change la salle d'un [Cours] d'un [prof] selon un [id] du [Cours] et un [roomId].
  Future<bool> changeCourseOfProfRoom({int id, int roomId}) async {
    Settings settings = await Settings.getConfiguration();
    final url =
        settings.etablissement.url + "fr/api/" + 'courses/scheduledcourses/$id';
    final response = await http.patch(Uri.parse(url),
        body: {
          "room": roomId,
        },
        headers: _headers);

    if (response.statusCode == 200) return true;
    return false;
  }

  // Récupère une [List] de [Room] diponibles selon un [roomtype], un [start] et un [end].
  // TODO 
  Future<List<Room>> getAvailableRooms(
      {String departement, int week, String day, String roomtype, int start, int end}) async {
    Settings settings = await Settings.getConfiguration();
    final url = settings.etablissement.url +
        "fr/api/" +
        'rooms/available/?dept_abbrev=$departement&roomtype=$roomtype&start=$start&end=$end';
    final response = await http.get(Uri.parse(url), headers: _headers);

    if (response.statusCode == 200)
      return Room.createListFromResponse(response);
    return <Room>[];
  }

  /// Récupère une [List] de [Tutor] selon un [departement]
  Future<List<Tutor>> getTutorsOfDepartment({String departement}) async {
    Settings settings = await Settings.getConfiguration();
    final url = settings.etablissement.url +
        "fr/api/" +
        'user/tutor/?dept=$departement';
    final response = await http.get(Uri.parse(url), headers: _headers);
    if (response.statusCode == 200)
      return Tutor.createListFromResponse(response);
    return <Tutor>[];
  }

  /// Récupère les [Promotion] d'un [department]
  /// Renvoie une liste de [Promotion]
  Future<List<Promotion>> getPromotions({String department}) async {
    Settings settings = await Settings.getConfiguration();
    final url = settings.etablissement.url +
        "fr/api/" +
        'base/trainingprogram/name/?dept=$department';
    final response = await http.get(Uri.parse(url), headers: _headers);
    if (response.statusCode == 200) {
      var res = jsonDecode(response.body);
      List<Promotion> promos = [];
      for (var promo in res) {
        promos.add(Promotion(
          department: department,
          name: promo['name'],
          promo: promo['abbrev'],
          groups:
              await getGroups(department: department, promo: promo['abbrev']),
        ));
      }
      return promos;
    }
    return <Promotion>[];
  }

  /// Récupère tous les groupes qui découlent d'un [department] et d'une [promo] précis
  ///
  /// Renvoie une [List] de groupes
  Future<List<String>> getGroups({String department, promo}) async {
    Settings settings = await Settings.getConfiguration();
    final url = settings.etablissement.url +
        "fr/api/" +
        'groups/structural/tree/?dept=$department';
    final response = await http.get(Uri.parse(url), headers: _headers);
    if (response.statusCode == 200) {
      var res = jsonDecode(response.body);
      List<String> groups = [];

      /// Structure tous les groupes récupéré selon un arbre
      if (promo != null) {
        for (var prom in res) {
          if (prom['promo'] == promo) {
            if (prom['children'] == null) {
              groups.add(prom['name']);
            } else {
              for (var child in prom['children']) {
                if (child['children'] != null) {
                  for (var childSub in child['children']) {
                    if (childSub['children'] != null) {
                      for (var childSubSub in childSub['children']) {
                        if (childSubSub['children'] == null) {
                          groups.add(childSubSub['name']);
                        }
                      }
                    } else {
                      groups.add(childSub['name']);
                    }
                  }
                } else {
                  groups.add(child['name']);
                }
              }
            }
          }
        }
      } else {
        for (var group in res) {
          groups.add(group['name']);
        }
      }
      return groups;
    }
    return null;
  }

  /// Crée une "Issue" sur le projet xFlop! sur Framagit en utilisant un [title]
  /// et une [description].
  /// Renvoie un [bool] selon le retour de l'API de GitLab
  Future<bool> createIssue(String title, String description) async {
    String url = 'https://framagit.org/api/v4/projects/78512/issues';
    Map<String, String> headers = {
      'PRIVATE-TOKEN': 'qpqWyNca2NVn9ndhy_PX',
      "Content-type": "application/json"
    };
    String body = jsonEncode(<String, String>{
      'id': "78512",
      'title': title,
      'issue_type': "incident",
      'description': description,
      'labels': "incident"
    });
    final response =
        await http.post(Uri.parse(url), headers: headers, body: body);
    return response.statusCode == 201;
  }

  ///Récupère une liste de [Incident] du projet xFlop! sur Framagit.
  ///Renvoie une liste d'[Incident].
  Future<List<Incident>> listIncidents() async {
    String url =
        'https://framagit.org/api/v4/projects/78512/issues?labels=incident&state=opened';
    final response = await http.get(Uri.parse(url), headers: {
      'PRIVATE-TOKEN': 'qpqWyNca2NVn9ndhy_PX',
      "Content-type": "application/json"
    });
    if (response.statusCode == 200)
      return Incident.createListFromResponse(response);
    return <Incident>[];
  }

  /// Connexion : Récupère le token de connexion grâce à [username] et [password]
  ///
  /// Lève une [AppException] lorsque le couple [username] et [password]
  /// est incorrecte ou lorsqu'il y a une erreur.
  Future<String> getConnectionToken(String username, String password) async {
    Settings settings = await Settings.getConfiguration();
    final url = settings.etablissement.url + 'fr/api/rest-auth/login/';
    final response = await http.post(Uri.parse(url),
        headers: _headers,
        body: jsonEncode(
            <String, String>{'username': username, 'password': password}));
    String token;
    if (response.statusCode == 200) {
      token = jsonDecode(response.body)['key'];
    } else if (response.statusCode == 400) {
      throw AppException("Informations de connexion incorrect", '');
    } else {
      throw AppException("Une erreur s'est produite", '');
    }
    return token;
  }

  /// Récupère les informations générales de l'utilisateur qui se connecte grâce au [token].
  /// Renvoie un [User]
  Future<User> getUserConnected(String token) async {
    Settings settings = await Settings.getConfiguration();
    final url = settings.etablissement.url + 'fr/api/rest-auth/user/';
    Map<String, String> headers = _headers;
    headers["Authorization"] = "Token " + token;
    final response = await http.get(Uri.parse(url), headers: headers);
    User user;
    if (response.statusCode == 200) {
      int id = jsonDecode(response.body)['pk'];
      final url = settings.etablissement.url + 'fr/api/user/users/$id/';
      final responseu = await http.get(Uri.parse(url), headers: _headers);
      if (responseu.statusCode == 200) {
        /// Si l'utilisatuer est un étudiant
        if (jsonDecode(responseu.body)['is_student']) {
          final url =
              settings.etablissement.url + 'fr/api/user/studentsinfo/$id/';
          final response = await http.get(Uri.parse(url), headers: _headers);
          if (response.statusCode == 200) {
            user = User.createUserFromResponse(response);
            user.isStudent = true;
            user.isTutor = false;
          }
        } else {
          ///Si l'utilisateur est un prof
          if (jsonDecode(responseu.body)['is_tutor']) {
            user = User.createUserFromResponse(response);
            user.isStudent = false;
            user.isTutor = true;
            id = jsonDecode(responseu.body)['departments'][0];
            final url =
                settings.etablissement.url + 'fr/api/fetch/alldepts/$id/';
            final responsee = await http.get(Uri.parse(url), headers: _headers);
            if (responsee.statusCode == 200) {
              user.departement = jsonDecode(responsee.body)['abbrev'];
            }
          } else {
            throw AppException("Votre compte n'est pas actif", 'Erreur : ');
          }
        }
      }
    }
    return user;
  }

  /// Récupère les informations d'un prof grâce à son [departement] et son [id]
  Future<Tutor> getInfoTutor(String departement, String id) async {
    Settings settings = await Settings.getConfiguration();
    final url =
        settings.etablissement.url + "fr/api/user/tutor/$id/?dept=$departement";
    final response = await http.get(Uri.parse(url));
    Tutor tutor;
    if (response.statusCode == 200) {
      tutor = Tutor.createTutorFromResponse(response);
    }
    return tutor;
  }

  ///Change le mot de passe de l'utilisateur en utilisant :
  ///le [token] comme identifiant ; le [password1] comme mot de passe
  ///
  ///Lève une [AppException] lorsque [password1] et [password2] ne corresponde pas.
  ///Lève une [AppException] lorsqu'une erreur se produit.
  postChangePassword(String token, String password1, String password2) async {
    Settings settings = await Settings.getConfiguration();
    final url =
        settings.etablissement.url + 'fr/api/rest-auth/password/change/';
    Map<String, String> headers = _headers;
    headers["Authorization"] = "Token " + token;
    final response = await http.post(Uri.parse(url),
        headers: _headers,
        body: jsonEncode(<String, String>{
          "new_password1": password1,
          "new_password2": password2
        }));
    if (response.statusCode == 400) {
      throw AppException("Les mots de passe ne correspondent pas", '');
    } else if (response.statusCode != 200) {
      throw AppException("Une erreur s'est produite", '');
    }
  }

  /// Envoie une demande de réinitialisation du mot de passe grâce au [mail].
  Future<String> postPasswordReset(String mail) async {
    Settings settings = await Settings.getConfiguration();
    final url = settings.etablissement.url + 'fr/api/rest-auth/password/reset/';
    final response = await http.post(Uri.parse(url),
        headers: _headers, body: jsonEncode(<String, String>{'email': mail}));
    if (response.statusCode == 200) {
      return response.statusCode.toString();
    }
    return response.statusCode.toString();
  }

  /// Supprimer le [token] courant et déconnecter l'utilisateur.
  Future<String> getLogout(String token) async {
    Settings settings = await Settings.getConfiguration();
    final url = settings.etablissement.url + 'fr/api/rest-auth/logout/';
    Map<String, String> headers = _headers;
    headers["Authorization"] = "Token " + token;
    final response = await http.post(Uri.parse(url), headers: headers);
    if (response.statusCode == 200) {
      return response.statusCode.toString();
    }
    return response.statusCode.toString();
  }
}
