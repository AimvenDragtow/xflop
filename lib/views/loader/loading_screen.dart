import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

// ignore: must_be_immutable
class LoadingScreen extends StatelessWidget {
  String msg = "";

  LoadingScreen(String msgLoSc) {
    this.msg = msgLoSc;
  }

  bool connexion() {
    return this.msg.compareTo(
            'Aucune connexion internet trouvé\nVérifiez votre connexion') !=
        0;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).primaryColor,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Image.asset(
            'assets/logo.png',
            width: 200,
          ),
          SizedBox(
            height: 50,
          ),
          Center(
              child: connexion()
                  ? SpinKitFadingFour(
                      color: Colors.white,
                    )
                  : Icon(
                      const IconData(983142, fontFamily: 'MaterialIcons'),
                      color: Colors.white,
                      size: 50,
                    )),
          SizedBox(
            height: 100,
          ),
          Text(
            msg,
            style: Theme.of(context).textTheme.button,
            textAlign: TextAlign.center,
          ),
        ],
      ),
    );
  }
}
