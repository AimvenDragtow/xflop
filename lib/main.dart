import 'dart:io';

import 'package:flop_edt_app/router/router.dart' as Custom;
import 'package:flop_edt_app/state_manager/state_widget.dart';
import 'package:flop_edt_app/theme/themes.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:provider/provider.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

Future main() async {
  await dotenv.load(fileName: '.env');
  await initializeDateFormatting("fr_FR", null);
  HttpOverrides.global = new MyHttpOverrides();
  runApp(StateWidget(child: XFlopApp()));
}

class XFlopApp extends StatefulWidget {
  @override
  _XFlopAppState createState() => _XFlopAppState();
}

class _XFlopAppState extends State<XFlopApp> {
  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
      create: (context) => ThemeProvider(),
      builder: (context, _) {
        final themeProvider = Provider.of<ThemeProvider>(context);
        themeProvider.getMode().then((value) {
          setState(() {
            themeProvider.themeMode = value;
          });
        });
        themeProvider.isDarkMode.then((value) {
          setState(() {
            value
                ? SystemChrome.setSystemUIOverlayStyle(
                    SystemUiOverlayStyle.light)
                : SystemChrome.setSystemUIOverlayStyle(
                    SystemUiOverlayStyle.dark);
          });
        });
        return MaterialApp(
          color: Color(0xFF07023B),
          debugShowCheckedModeBanner: false,
          title: 'xFlop!',
          theme: AppTheme.lightTheme(),
          darkTheme: AppTheme.darkTheme(),
          themeMode: themeProvider.themeMode,
          routes: {
            '/': (context) => Custom.Router(),
          },
        );
      });
}
