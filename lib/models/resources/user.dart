import 'dart:convert';
import 'package:flop_edt_app/models/resources/etablissement.dart';
import 'package:flop_edt_app/models/state/settings.dart';
import '../../api/api_provider.dart';

///Classe modèle [User] permettant de stocker un utilisateur
///complète (username, email, password).
class User {
  String id;
  String username;
  String email;
  String firstName;
  String lastName;
  String initials;
  String departement;
  String promo;
  String groupe;
  Etablissement etablissement;
  String password;
  String token;
  bool isStudent;
  bool isTutor;

  ///Constructeur d'un utilisateur.
  User(
      {this.id,
      this.username,
      this.email,
      this.firstName,
      this.lastName,
      this.initials,
      this.etablissement,
      this.departement,
      this.promo,
      this.groupe,
      this.password,
      this.token,
      this.isStudent,
      this.isTutor});

  ///Crée une [Map] à partir de l'objet.
  ///A Completer
  Map<String, dynamic> get toMap => {
        'id': this.id,
        'username': this.username,
        'email': this.email,
        'firstName': this.firstName,
        'lastName': this.lastName,
        'initials': this.initials,
        'etablissement': this.etablissement?.toMap,
        'departement': this.departement,
        'promo': this.promo,
        'groupe': this.groupe,
        'password': this.password,
        'token': this.token,
        'isStudent': this.isStudent,
        'isTutor': this.isTutor,
      };

  ///Retourne une chaîne JSON de l'objet.
  String get toJSON => jsonEncode(this.toMap);

  factory User.fromJSON(Map<String, dynamic> json) => User(
      id: json["pk"] == null ? json["id"] : json["pk"].toString(),
      username: json["username"],
      email: json["email"],
      firstName:
          json["first_name"] == null ? json["firstName"] : json["first_name"],
      lastName:
          json["last_name"] == null ? json["lastName"] : json["last_name"],
      initials: json["initials"] == null
          ? (json["first_name"] == null
                      ? json["firstName"]
                      : json["first_name"])
                  .substring(0, 1) +
              (json["last_name"] == null ? json["lastName"] : json["last_name"])
                  .substring(0, 1)
          : json["initials"],
      departement: json["departement"] == null
          ? json["department"]
          : json["departement"],
      promo: json["promo"] == null ? json["train_prog"] : json["promo"],
      groupe: json["groupe"] == null
          ? json["groups"] == null
              ? null
              : json["groups"][0]["name"]
          : json["groupe"],
      password: json["password"],
      token: json["token"],
      etablissement: json["etablissement"] == null
          ? null
          : Etablissement.fromJSON(json['etablissement']),
      isStudent: json["isStudent"],
      isTutor: json["isTutor"]);

  setDepartement(String departement) {
    this.departement = departement;
  }

  setPromo(String promo) {
    this.promo = promo;
  }

  setGroupe(String groupe) {
    this.groupe = groupe;
  }

  static User createUserFromResponse(response) {
    var json = jsonDecode(utf8.decode(response.bodyBytes));
    return User.fromJSON(json);
  }

  ///Méthode d'affichage de [User]
  @override
  String toString() {
    return username + ' - ' + firstName + ' ' + lastName + ' - ' + email;
  }

  static Future<User> seConnecter(String username, String password) async {
    User user;
    APIProvider api = APIProvider();
    String token = await api.getConnectionToken(username, password);
    if (token == null) {
    } else {
      user = await api.getUserConnected(token);
      user.token = token;
      user.password = password;
      Settings settings = await Settings.getConfiguration();
      if (user.isTutor) {
        settings.isTutor = user.isTutor;
        settings.tutor = await api.getInfoTutor(user.departement, user.id);
        user.initials = settings.tutor.initiales;
      } else if (user.isStudent) {
        user.isStudent = false;
        settings.department = user.departement;
        settings.groupe = user.groupe;
        settings.promo = user.promo;
      }
      settings.user = user;
      settings.user.etablissement = settings.etablissement;

      settings.saveConfiguration();
    }
    return user;
  }

  static passwordForgot(String mail) async {
    APIProvider api = APIProvider();
    await api.postPasswordReset(mail);
  }

  static postChangePassword(
      String token, String password1, String password2) async {
    APIProvider api = APIProvider();
    await api.postChangePassword(token, password1, password2);
  }
}
