import 'dart:convert';

import 'package:http/http.dart';
import 'package:flutter/material.dart';
import 'dart:ui' as ui;

class Incident {
  int id;
  String titre;
  String description;
  bool state;
  List<dynamic> labels;
  bool enDev;

  Incident({this.id, this.titre, this.description, this.state, this.labels});

  factory Incident.fromJSON(Map<String, dynamic> json) => Incident(
        id: json['id'],
        titre: json['title'],
        description: json['description'],
        state: json['state'] == 'opened' ? true : false,
        labels: json['labels'],
      );

  Map<String, dynamic> get toMap => {
        'id': this.id,
        'titre': this.titre,
        'description': this.description,
        'state': this.state,
        'labels': this.labels,
      };

  ///Retourne une chaîne JSON de l'objet.
  String get toJSON => jsonEncode(this.toMap);

  ///Méthode d'affichage de [Incident]
  @override
  String toString() {
    return this.toJSON;
  }

  ///Crée une liste de [Incident] à partir de la réponse API.
  static List<Incident> createListFromResponse(Response response) {
    var incidents = jsonDecode(utf8.decode(response.bodyBytes));
    var toReturn = <Incident>[];
    incidents.forEach((dynamic json) => toReturn.add(Incident.fromJSON(json)));
    toReturn.forEach((incid) {
      incid.labels.forEach((label) {
        label == "En développement"
            ? incid.enDev = true
            : incid.enDev = incid.enDev ?? false;
      });
    });
    return toReturn;
  }

  void displayInformations(BuildContext context) {
    showModalBottomSheet(
      context: context,
      backgroundColor: Colors.transparent,
      builder: (context) {
        return ClipRRect(
            borderRadius: BorderRadius.all(Radius.circular(50.0)),
            child: Stack(
              children: [
                BackdropFilter(
                  filter: new ui.ImageFilter.blur(
                    sigmaX: 7.0,
                    sigmaY: 7.0,
                  ),
                  child: Container(),
                ),
                Container(
                  decoration: BoxDecoration(
                    color: Color(0xFFBD0026).withOpacity(0.4),
                    borderRadius: BorderRadius.all(Radius.circular(50.0)),
                  ),
                  child: Column(
                    children: <Widget>[
                      Padding(padding: EdgeInsets.all(2)),
                      Text('Incident',
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 22,
                            color: Colors.white,
                          )),
                      Padding(
                        padding: EdgeInsets.only(left: 20, right: 20),
                        child: Text(
                          this.titre,
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.white,
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 16,
                      ),
                      Container(
                          padding: EdgeInsets.all(15),
                          decoration: BoxDecoration(
                            color: Color(0xFFBD0026),
                            borderRadius:
                                BorderRadius.all(Radius.circular(20.0)),
                            boxShadow: [
                              BoxShadow(
                                color: Color(0xFFBD0026).withOpacity(0.5),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset: Offset(0, 3),
                              ),
                            ],
                          ),
                          child: Column(children: [
                            Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Icon(
                                    const IconData(63131,
                                        fontFamily: 'MaterialIcons'),
                                    color: Colors.white,
                                  ),
                                  Text(
                                    'Description',
                                    style: TextStyle(
                                      fontSize: 16,
                                      color: Colors.white,
                                    ),
                                  ),
                                ]),
                            SizedBox(
                              height: 5,
                            ),
                            Text(
                              this.description,
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.white,
                              ),
                            ),
                          ])),
                      SizedBox(
                        height: 20,
                      ),
                      Container(
                          padding: EdgeInsets.all(15),
                          decoration: BoxDecoration(
                            color: this.enDev
                                ? Color(0xFFea8f00)
                                : Color(0xFFBD0026),
                            borderRadius:
                                BorderRadius.all(Radius.circular(20.0)),
                            boxShadow: [
                              BoxShadow(
                                color: this.enDev
                                    ? Color(0xFFea8f00).withOpacity(0.5)
                                    : Color(0xFFBD0026).withOpacity(0.5),
                                spreadRadius: 5,
                                blurRadius: 7,
                                offset: Offset(0, 3),
                              ),
                            ],
                          ),
                          child: Column(children: [
                            Text(
                              this.enDev
                                  ? "En cours de traitement ..."
                                  : "Pas encore traité.",
                              style: TextStyle(
                                fontSize: 16,
                                color: Colors.white,
                              ),
                            ),
                          ])),
                    ],
                  ),
                ),
              ],
            ));
      },
    );
  }
}
