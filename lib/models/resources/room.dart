import 'dart:convert';

import 'package:http/http.dart';

///Classe [Room] permettant de représenter en objet
///un cours retourné par l'API.
class Room {
  ///[id] de la salle.
  final int id;

  ///[name] de la salle.
  final String name;

  ///[roomtype] de la salle.
  final String roomtype;

  @override
  String toString() {
    return '<Room $id - $name: type $roomtype>';
  }

  Room({this.id, this.name, this.roomtype});

  ///Constructeur Factory depuis un JSON.
  factory Room.fromJson(Map<String, dynamic> json) {
    return Room(
      id: json['id'],
      name: json['name'],
      roomtype: json['roomtype'],
    );
  }

  ///Crée une liste de [Room] à partir de la réponse API.
  static List<Room> createListFromResponse(Response response, year, week) {
    var json = jsonDecode(utf8.decode(response.bodyBytes));
    var toReturn = <Room>[];
    json.forEach((dynamic json) => toReturn.add(Room.fromJSON(json)));
    return toReturn;
  }
}
