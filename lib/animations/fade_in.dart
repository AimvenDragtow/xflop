import 'package:flutter/material.dart';
import 'package:simple_animations/simple_animations.dart';
import 'package:sa4_migration_kit/sa4_migration_kit.dart';

enum AniProps { opacity, translateX }

///[Widget] permettant de créer l'animation lors de l'affichage des cours
class FadeIn extends StatelessWidget {
  final double delay;
  final Widget child;

  FadeIn(this.delay, this.child);

  @override
  Widget build(BuildContext context) {
    final tween = MultiTween<AniProps>()
      ..add(AniProps.opacity, Tween(begin: 0.0, end: 1.0),
          Duration(milliseconds: 450))
      ..add(AniProps.translateX, Tween(begin: 130.0, end: 0.0),
          Duration(milliseconds: 450), Curves.easeOut);

    return PlayAnimation<MultiTweenValues<AniProps>>(
      delay: Duration(milliseconds: (300 * delay).round()),
      duration: tween.duration,
      tween: tween,
      child: child,
      builder: (context, child, animation) => Opacity(
        opacity: animation.get(AniProps.opacity),
        child: Transform.translate(
            offset: Offset(animation.get(AniProps.translateX), 0),
            child: child),
      ),
    );
  }
}

//import 'package:flutter/material.dart'; import 'package:simple_animations/simple_animations.dart'; enum AniProps { opacity, translateX } ///[Widget] permettant de créer l'animation lors de l'affichage des cours class FadeIn extends StatelessWidget { final double delay; final Widget child; FadeIn(this.delay, this.child); @override Widget build(BuildContext context) { final tween = MovieTween() ..tween('opacity', Tween(begin: 0.0, end: 1.0), duration: const Duration(milliseconds: 450)) .thenTween('translateX', Tween(begin: 130, end: 0.0), duration: const Duration(milliseconds: 450), curve: Curves.easeOut); return PlayAnimationBuilder<Movie>( tween: tween, duration: tween.duration, // for 1 second builder: (context, value, child) => Opacity( opacity: value.get('opacity'), child: Transform.translate( offset: Offset(value.get('translateX').toDouble(), 0), child: child), ), ); } }
